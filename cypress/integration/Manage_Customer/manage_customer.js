import{Given, When, Then} from 'cypress-cucumber-preprocessor/steps';
import AddCustomerPageObject from '../../support/PageObject/Add_Customer_PageObject';
import CommonPageObject from '../../support/PageObject/Common_PageObject';
import CustomerPageObject from '../../support/PageObject/Customer_Page_Object';
import LoginPageObject from '../../support/PageObject/Login_PageObject';
import OpenAccountPageObject from '../../support/PageObject/Open_Account_PageObject';

const common = new CommonPageObject();
const addCustomer = new AddCustomerPageObject();
const login = new LoginPageObject();
const customer = new CustomerPageObject();
const openAC = new OpenAccountPageObject();

Given("user navigate to site url",()=>{
    common.navigateToUrl();
})

Then("login as bank manager",()=>{
    login.loginAsBankManager();
})

When("click on customer menu tab",()=>{
    customer.clickOnCustomerMenuTab();
})

Then("verify {string} customer detail is not available into list",(firstname)=>{
    customer.verifyCustomerNameIsNotAvailableIntoList(firstname);
})

When("click on add a customer",()=>{
    addCustomer.clickOnAddCustomerMenuTab();
})

Then("enter customer {string} into first name field",(firstname)=>{
    addCustomer.getFirstNameField().type(firstname);
})

Then("enter customer {string} into last name field",(lastname)=>{
   addCustomer.getLastNameField().type(lastname); 
})

Then("enter {string} into post code field",(postcode)=>{
   addCustomer.getPostCodeField().type(postcode);
})

Then("click on add customer submit",()=>{
   addCustomer.addCustomerSubmit().click({force:true});
})

Then("verify {string} into firstname and {string} into lastname customer detail is available into list",(firstname,lastname)=>{
   customer.verifyCustomerNameIsAvailableIntoList(firstname,lastname);
})


Then("click on home button",()=>{
   common.getHomeButton();
})

Then("login as customer",()=>{
   login.loginAsCustomer();
})
Then("select {string} as user name",(username)=>{
   login.getUserNameSelect(username);
})

Then("click on login button",()=>{
   login.getLoginButton();
})

Then("verify {string} user is logged in",(username)=>{
   login.verifyUserIsLoggedIn(username);
})

Then("delete customer record where username is {string}",(username)=>{
   customer.getCustomerRecordDelete(username);
})

Then("click on open account menu tab",()=>{
   openAC.clickOnOpenAccountMenuTab();
})

Then("select {string} as customer name",(username)=>{
   openAC.selectCustomerName(username);
})

Then("select {string} as currency type",(currencyType)=>{
   openAC.selectCurrencyType(currencyType);
})

Then("click on proceed button",()=>{
   openAC.clickOnProcessButton();
})

Then("store the account number of user {string}",(username)=>{
   customer.storeTheAccountNumberOfUser(username);
})

Then("verify the account number of user",()=>{
   customer.verifyTheAccountNumber();
})

Then("logout as customer",()=>{
    login.getLogoutButton();
})



