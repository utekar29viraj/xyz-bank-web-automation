Feature: XYX Bank Application Web Automation

@focus
Scenario: verify new customer record is added
Given user navigate to site url 
Then login as bank manager
When click on customer menu tab
Then verify "test" customer detail is not available into list
When click on add a customer
Then enter customer "test" into first name field
Then enter customer "test" into last name field
Then enter "410205" into post code field
Then click on add customer submit
When click on customer menu tab
Then verify "test" into firstname and "test" into lastname customer detail is available into list
Then click on home button
Then login as customer
Then select "test test" as user name
Then click on login button
Then verify "test test" user is logged in
Then logout as customer

@focus
Scenario: verify newly added customer record is removed
Given user navigate to site url 
Then login as bank manager
When click on customer menu tab
Then verify "test" customer detail is not available into list
When click on add a customer
Then enter customer "test" into first name field
Then enter customer "test" into last name field
Then enter "410205" into post code field
Then click on add customer submit
When click on customer menu tab
Then delete customer record where username is "test"
Then verify "test" customer detail is not available into list

@focus
Scenario: verify new account for customer is created
Given user navigate to site url 
Then login as bank manager
When click on customer menu tab
Then verify "test" customer detail is not available into list
When click on add a customer
Then enter customer "test" into first name field
Then enter customer "test" into last name field
Then enter "410205" into post code field
Then click on add customer submit
When click on customer menu tab
Then verify "test" into firstname and "test" into lastname customer detail is available into list
Then click on open account menu tab
Then select "test test" as customer name
Then select "Rupee" as currency type
Then click on proceed button
When click on customer menu tab
Then store the account number of user "test"
Then click on home button
Then login as customer
Then select "test test" as user name
Then click on login button
Then verify the account number of user









 



