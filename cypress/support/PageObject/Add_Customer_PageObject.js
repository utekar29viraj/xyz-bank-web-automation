/// <reference types="cypress" />

var user = require('../../fixtures/user.json');

export default class AddCustomerPageObject{
 
    clickOnAddCustomerMenuTab(){
        cy.get(user.bank_manager_dashboard_elements.add_customer_menu_button,{timeout:10000}).should('be.visible');
        cy.get(user.bank_manager_dashboard_elements.add_customer_menu_button,{timeout:10000}).click({force:true});
        cy.get(user.bank_manager_dashboard_elements.add_customer_menu_button,{timeout:10000}).should('be.enabled');
    }

    getFirstNameField(){
        cy.get(user.add_customer_elements.first_name_field,{timeout:10000}).should('be.visible');
        return cy.get(user.add_customer_elements.first_name_field,{timeout:10000});
    }

    getLastNameField(){
        cy.get(user.add_customer_elements.last_name_field,{timeout:10000}).should('be.visible');
        return cy.get(user.add_customer_elements.last_name_field,{timeout:10000});
    }

    getPostCodeField(){
        cy.get(user.add_customer_elements.post_code_field,{timeout:10000}).should('be.visible');
        return cy.get(user.add_customer_elements.post_code_field,{timeout:10000});
    }

    addCustomerSubmit(){
        cy.get(user.add_customer_elements.add_customer_submit,{timeout:10000}).should('be.visible');
        return cy.get(user.add_customer_elements.add_customer_submit,{timeout:10000});
    }
}