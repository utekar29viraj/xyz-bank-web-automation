/// <reference types="cypress" />

var user = require('../../fixtures/user.json');
var firstNameVal = "";
var  lastNameVal = "";
var accountNumber = 0;
export default class CustomerPageObject{
    clickOnCustomerMenuTab(){
        cy.get(user.bank_manager_dashboard_elements.customer_menu_button,{timeout:10000}).should('be.visible');
        cy.get(user.bank_manager_dashboard_elements.customer_menu_button,{timeout:10000}).click({force:true});
        cy.get(user.bank_manager_dashboard_elements.customer_menu_button,{timeout:10000}).should('be.enabled');
    }

    verifyCustomerNameIsNotAvailableIntoList(firstname){
        cy.get(user.customer_elements.search_bar_field,{timeout:100000}).should('be.visible');
        cy.get(user.customer_elements.search_bar_field,{timeout:100000}).clear();
        cy.get(user.customer_elements.search_bar_field,{timeout:10000}).should('be.visible');
        cy.get(user.customer_elements.search_bar_field,{timeout:10000}).type(firstname);
        cy.get(user.customer_elements.customer_list,{timeout:10000}).then((listName)=>{
            if(listName.text().toString().includes("")){
             cy.get(user.customer_elements.customer_list,{timeout:10000}).invoke('val').should('be.empty');
            }
         })
    }

    verifyCustomerNameIsAvailableIntoList(firstName,lastName){
        cy.get(user.customer_elements.search_bar_field,{timeout:10000}).should('be.visible');
        cy.get(user.customer_elements.search_bar_field,{timeout:10000}).type(firstName);
        cy.get(user.customer_elements.customer_list_with_value,{timeout:10000}).then((customerRowList)=>{
                var rowIndex = customerRowList.length;
                 cy.get("div.ng-scope > div > div:nth-Child(2) > div > div:nth-Child(2) > table > tbody > tr:nth-Child("+rowIndex+") > td:nth-Child(1)").then((firstNameValue)=>{
                     firstNameVal =  firstNameValue.text().toString();
                    if(firstNameVal.includes(firstName)){
                        expect(firstNameVal).contains(firstName);
                    }
                })

                cy.get("div.ng-scope > div > div:nth-Child(2) > div > div:nth-Child(2) > table > tbody > tr:nth-Child("+rowIndex+") > td:nth-Child(2)").then((lastNameValue)=>{
                    lastNameVal = lastNameValue.text().toString();
                    if(lastNameVal.includes(lastName)){
                        expect(lastNameVal).contains(lastName);
                   }
               })
           })
    }


    getCustomerRecordDelete(username){
        cy.get(user.customer_elements.search_bar_field,{timeout:100000}).should('be.visible');
        cy.get(user.customer_elements.search_bar_field,{timeout:100000}).type(username);
        cy.get(user.customer_elements.customer_list_with_value,{timeout:10000}).then((customerNameList)=>{
            var rowIndex = customerNameList.length;
            cy.get("div.ng-scope > div > div:nth-Child(2) > div > div:nth-Child(2) > table > tbody > tr:nth-Child("+rowIndex+") > td:nth-Child(1)").should('be.visible');
            cy.get("div.ng-scope > div > div:nth-Child(2) > div > div:nth-Child(2) > table > tbody > tr:nth-Child("+rowIndex+") > td:nth-Child(1)").then((firstNameVal)=>{
                if(firstNameVal.text().toString().includes(username)){
                    cy.get("div.ng-scope > div > div:nth-Child(2) > div > div:nth-Child(2) > table > tbody > tr:nth-Child("+rowIndex+") > td:nth-Child(5) > button").should('be.visible');
                    cy.get("div.ng-scope > div > div:nth-Child(2) > div > div:nth-Child(2) > table > tbody > tr:nth-Child("+rowIndex+") > td:nth-Child(5) > button").click({force:true});
                }
            }) 
        })
    }

    storeTheAccountNumberOfUser(username){
        cy.get(user.customer_elements.search_bar_field,{timeout:10000}).should('be.visible');
        cy.get(user.customer_elements.search_bar_field,{timeout:10000}).type(username);
        cy.get(user.customer_elements.customer_list_with_value).then((customerNameList)=>{
            var rowIndex = customerNameList.length;
            cy.get("div.ng-scope > div > div:nth-Child(2) > div > div:nth-Child(2) > table > tbody > tr:nth-Child("+rowIndex+") > td:nth-Child(1)").then((customerFirstNameValue)=>{
                var firstNameValue = customerFirstNameValue.text().toString();
                  if(firstNameValue.includes(username)){
                    cy.get("div.ng-scope > div > div:nth-Child(2) > div > div:nth-Child(2) > table > tbody > tr:nth-Child("+rowIndex+") > td:nth-Child(4)").then((accountNumberValue)=>{
                        accountNumber = parseInt(accountNumberValue.text().toString().trim());
                
                    })
                  }
            })  
        })
    }

    verifyTheAccountNumber(){
        cy.get("div.ng-scope > div.padT20 > div:nth-Child(3) > strong").then((fieldVal)=>{
            var rowIndex = fieldVal.length-2;
            cy.get("div.ng-scope > div.padT20 > div:nth-Child(3) > strong:nth-Child("+rowIndex+")").then((accountNumberValue)=>{
                var accountVal = parseInt(accountNumberValue.text().toString().trim());
                expect(accountVal).equals(accountNumber);
            })
        })
    }

}