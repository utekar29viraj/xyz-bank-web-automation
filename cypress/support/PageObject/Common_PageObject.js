/// <reference types="cypress" />

var user = require('../../fixtures/user.json');

export default class CommonPageObject{
  
    navigateToUrl(){
        cy.visit(user.site.site_url,{timeout:10000});
    }

    getHomeButton(){
        cy.get(user.login.home_button,{timeout:10000}).should('be.visible');
        cy.get(user.login.home_button,{timeout:10000}).click({force:true});
    }
}