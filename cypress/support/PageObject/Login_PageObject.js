/// <reference types="cypress" />

var user = require('../../fixtures/user.json');

export default class LoginPageObject{

    loginAsBankManager(){
        cy.get(user.login.bank_manage_login,{timeout:10000}).should('be.visible');
        cy.get(user.login.bank_manage_login,{timeout:10000}).click({force:true});
        cy.get(user.bank_manager_dashboard_elements.home_button,{timeout:10000}).should('be.visible');
        cy.get(user.bank_manager_dashboard_elements.home_button,{timeout:10000}).then((buttonText)=>{
            expect(buttonText.text().toString()).contains(user.bank_manager_dashboard_data.label_home);
        })
    }

    loginAsCustomer(){
        cy.get(user.login.customer_login,{timeout:10000}).should('be.visible');
        cy.get(user.login.customer_login,{timeout:10000}).click({force:true});
    }

    getUserNameSelect(username){

        cy.get(user.login.select_username,{timeout:10000}).should('be.visible');
        cy.get(user.login.select_username,{timeout:10000}).select(username);
        cy.get(user.login.select_username,{timeout:10000}).should('contain.text',user.login_data.label_username);        
    }

    getLoginButton(){
        cy.get(user.login.login_button,{timeout:10000}).should('be.visible');
        cy.get(user.login.login_button,{timeout:10000}).click({force:true});
    }

    getLogoutButton(){
        cy.get(user.login.logout_button,{timeout:10000}).should('be.visible');
        cy.get(user.login.logout_button,{timeout:10000}).click({force:true});   
    }

    verifyUserIsLoggedIn(username){
        cy.get(user.login.dashboard_title,{timeout:10000}).should('be.visible');
        cy.get(user.login.dashboard_title,{timeout:10000}).then((titleValue)=>{
            var dashboardTitleValue = titleValue.text().toString().trim();
             expect(dashboardTitleValue).equals(username);
        })
    }

}