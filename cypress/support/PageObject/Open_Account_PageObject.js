
/// <reference types="cypress" />

var user = require('../../fixtures/user.json');

export default class OpenAccountPageObject{

    clickOnOpenAccountMenuTab(){
        cy.get(user.bank_manager_dashboard_elements.open_account_menu_button,{timeout:10000}).should('be.visible');
        cy.get(user.bank_manager_dashboard_elements.open_account_menu_button,{timeout:10000}).click({force:true});
        cy.get(user.bank_manager_dashboard_elements.open_account_menu_button,{timeout:10000}).should('be.enabled');
    }

    selectCustomerName(username){
        cy.get(user.open_account_elements.customer_name_drop_down,{timeout:10000}).should('be.visible');
        cy.get(user.open_account_elements.customer_name_drop_down,{timeout:10000}).select(username);
    }

    selectCurrencyType(currencyType){
        cy.get(user.open_account_elements.currency_type_drop_down,{timeout:10000}).should('be.visible');
        cy.get(user.open_account_elements.currency_type_drop_down,{timeout:10000}).select(currencyType);
    }

    clickOnProcessButton(){
        cy.get(user.open_account_elements.process_button,{timeout:10000}).should('be.visible');
        cy.get(user.open_account_elements.process_button,{timeout:10000}).click({force:true});
    }
}